<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */

$this->title = $model->codigo_cliente;
//$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//\yii\web\YiiAsset::register($this);
?>
<div class="pt-5"></div>
<div class="clientes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'codigo_cliente' => $model->codigo_cliente], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'codigo_cliente' => $model->codigo_cliente], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de que quieres eliminar este cliente?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_cliente',
            'nombre',
            'apellidos',
            'telefono',
            'email:email',
            'direccion',
            'dni',
        ],
    ]) ?>

</div>
