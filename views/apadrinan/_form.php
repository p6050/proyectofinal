<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Apadrinan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="apadrinan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_animal')->textInput() ?>

    <?= $form->field($model, 'codigo_cliente')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn colorboton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
