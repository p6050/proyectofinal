<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Apadrinan */

$this->title = 'Actualizar apadrinador: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Apadrinans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="apadrinan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
