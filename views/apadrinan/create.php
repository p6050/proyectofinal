<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Apadrinan */

$this->title = 'Crear un apadrinanador';
$this->params['breadcrumbs'][] = ['label' => 'Apadrinans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="apadrinan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
