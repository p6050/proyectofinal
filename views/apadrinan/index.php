<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Animales apadrinados';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pt-5"></div>
<div class="apadrinan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Animal apadrinado', ['create'], ['class' => 'btn btn colorboton']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'codigo_animal',
//            'codigo_cliente',
            
                [
                'attribute'=>'nombre del cliente',
                'value'=>'codigoCliente.nombre',
                 
                ],
                 [
                'attribute'=>'apellidos del cliente',
                'value'=>'codigoCliente.apellidos',
                ],
            [
                'attribute'=>'telefono',
                'value'=>'codigoCliente.telefono',
                ],
                 [
                'attribute'=>'nombre_mascosta',
                'value'=>'codigoAnimal.nombre_mascota',
                ],
                 [
                'attribute'=>'tipo de animal',
                'value'=>'codigoAnimal.tipo_de_animal',
                ],
             
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
