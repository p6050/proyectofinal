<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/imagenes/nuevofavicon2.png" type="image/x-icon" />
    
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100 fondo">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
       
        'brandLabel' => Html::img('@web/imagenes/logodefinitivo2.png',['style'=>'height:60px;'],['class'=>'navbar-brand'],['alt' => 'logo2']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar my-navbar navbar-expand-md navbar-light .navbar-nav fixed-top ',
        ],
    ]);
    
    
    if(Yii::$app->user->isGuest){
            echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-light'],
            'items' => [
                ['label' => 'Animales', 'url' => ['/site/todoslosanimales']],
                ['label' => 'Protectoras', 'url' => ['/site/protectoras']],
                ['label' => 'Apadrinados', 'url' => ['/site/animalesapadrinados']],
                ['label' => 'Animales devueltos', 'url' => ['/site/animalesdevueltos']],
               
                ['label' => 'Acceso', 'url' => ['/site/login']]
            ],
        ]);
        }else{
              echo Nav::widget([
        'options' => ['class' => 'navbar-nav '],
        'items' => [   
            ['label' => 'Animales', 'url' => ['/site/todoslosanimales']],
            ['label' => 'Protectoras', 'url' => ['/site/protectoras']],
            ['label' => 'Apadrinados', 'url' => ['/site/animalesapadrinados']],
            ['label' => 'Animales devueltos', 'url' => ['/site/animalesdevueltos']],
            
            ['label' => 'Información', 'url' => ['/site/cruds']],
           
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                . Html::submitButton(
                    'Acceso (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            
        ],
        ]);
        }
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted ">
    <div class="container">
        <p class="float-left">&copy;Adopta&Rescata</p>
        <p class="float-right">Contacto: informacion@Adopta&rescata.com</p>
        
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
