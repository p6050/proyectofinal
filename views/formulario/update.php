<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Formulario */

$this->title = 'Actualizando formulario: ' ;     // . $model->id
//$this->params['breadcrumbs'][] = ['label' => 'Formularios', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class='pt-5'></div>
<div class="formulario-update">

    <h1><?= Html::encode($this->title) ?></h1>
<div class='pt-3'></div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
