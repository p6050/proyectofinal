<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Formulario */

$this->title = 'Crear Formulario';
//$this->params['breadcrumbs'][] = ['label' => 'Formularios', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class='pt-5'></div>
<div class="formulario-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class='pt-3'></div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
