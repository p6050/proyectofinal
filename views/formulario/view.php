<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Formulario */
$this->title = 'Datos del Formulario ' ;
//$this->title = $model->id;
//$this->params['breadcrumbs'][] = ['label' => 'Formularios', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//\yii\web\YiiAsset::register($this);
?>
<div class='pt-5'></div>
<div class="formulario-view">

    <h1><?= Html::encode($this->title) ?></h1>
<div class='pt-3'></div>
    <p>
        <?= Html::a('Actualizar datos', ['update', 'id' => $model->id], ['class' => 'btn btn colorboton']) ?>
<!--         <Html::a('Ver pdf', ['report', 'id' => $model->id], ['class' => 'btn btn colorboton']) -->
      <?=   Html::a('Ver pdf', ['report','id'=>$model->id], ['class' => "btn btn colorboton",'target'=>'_blank'])  ?>

        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
//            'codigo_animal',
            'nombre_usuario',
            'apellido_usuario',
            'telefono_usuario',
            'correo',
        ],
    ]) ?>

</div>
