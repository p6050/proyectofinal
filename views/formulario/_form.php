<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Formulario */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Formulario';
?>

<div class="formulario-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'nombre_usuario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellido_usuario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefono_usuario')->textInput() ?>

    <?= $form->field($model, 'correo')->textInput(['maxlength' => true]) ?>
    

    <div class="form-group">
        <?= Html::submitButton('Continuar', ['class' => 'btn btn colorboton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
