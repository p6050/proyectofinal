<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Veterinarios';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pt-5"></div>
<div class="veterinarios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Veterinarios', ['create'], ['class' => 'btn btn colorboton']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'codigo_veterinario',
            'nombre',
            'apellidos',
            'numero_profesional_veterinario',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action,  $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_veterinario' => $model->codigo_veterinario]);
                 }
            ],
        ],
    ]); ?>


</div>
