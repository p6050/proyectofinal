<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Veterinarios */

$this->title = $model->codigo_veterinario;
//$this->params['breadcrumbs'][] = ['label' => 'Veterinarios', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//\yii\web\YiiAsset::register($this);
?>
<div class="pt-5"></div>
<div class="veterinarios-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigo_veterinario' => $model->codigo_veterinario], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigo_veterinario' => $model->codigo_veterinario], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de que quieres eliminar este veterinario?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_veterinario',
            'nombre',
            'apellidos',
            'numero_profesional_veterinario',
        ],
    ]) ?>

</div>
