<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Veterinarios */

$this->title = 'Actualizar Veterinarios: ' . $model->codigo_veterinario;
//$this->params['breadcrumbs'][] = ['label' => 'Veterinarios', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->codigo_veterinario, 'url' => ['view', 'codigo_veterinario' => $model->codigo_veterinario]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pt-5"></div>
<div class="veterinarios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
