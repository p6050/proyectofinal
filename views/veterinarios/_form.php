<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Veterinarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="veterinarios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numero_profesional_veterinario')->textInput() ?>

    <div class="form-group">
       <?= Html::submitButton('Guardar', ['class' => 'btn btn colorboton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
