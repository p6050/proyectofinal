<?php

// 

use yii\widgets\ListView;
use yii\helpers\Html;
use yii\bootstrap4\Dropdown;

$this->title = 'Animales devueltos';
?>
<div class ="pt-5 "></div>
<h1 class="display-4 titulo" style="padding-top: 15px">Animales devueltos</h1>
<div class="site-index">
    <div class="body-content solucioncontent ">      

        <div class="btn-group">
            <button type="button" class="btn btn colorboton dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Filtrar preferencias
            </button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="opcion">Todos</a>
                <a class="dropdown-item" href="opcion1">Perros</a>
                <a class="dropdown-item " href="opcion2">Gatos</a>


            </div>
        </div>


        <div class="pt-5"> </div>

        <div class="row  ">


            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}",
                'itemView' => function($model) {
                    ?>


                    <div class=" col-md-6"> 
                        <div class="row polaroid">


                            <!--                        <div class="card col-6"> la tarjeta queda dividida en dos-->
                            <div class=" col-6 alturaminima2 margenesimagen">

                                <?php
                                if ($model->tipo_de_animal == 'Gato' || 'gato') {

                                    echo Html::img('@web/imagenes/' . $model->raza . '.png', ['width' => '100%', 'height' => '100%'], ['alt' => 'My logo'], ['class' => 'card-img-bottom']);
                                } elseif ($model->tipo_de_animal == 'Perro' || 'perro') {

                                    echo Html::img('@web/imagenes/' . $model->raza . '.png', ['width' => '100%', 'height' => '100%'], ['alt' => 'My logo'], ['class' => 'card-img-bottom']);
                                }
                                ?>

                            </div>
                            <!--                         <div class="card col-6 ">--> 



                            <div class=" col-5  centrartarjeta margenestexto">
                                <h5 class="card-title"><b>Datos de interés</b></h5>



                                <div>

                                    <?= "<b>Protectora: </b>" . $model->nombre_protectora ?>

                                </div>
                                <div>

                                    <?= "<b> Nombre: </b> " . $model->nombre_mascota ?>

                                </div>

                                <div>
                                    <?= "<b>Tipo: </b>" . $model->tipo_de_animal ?>

                                </div>
                                <div>
                                    <?= "<b>Edad: </b>" . $model->edad . " años" ?>
                                </div>



                                <br>


                                <?= Html::a('Adoptar',  ['/formulario/create','animal'=>$model->codigo_animal], ['class' => 'btn btn colorboton']) ?>

                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn colorboton mprueba" data-toggle="modal" data-target="#exampleModalCenter" data-raza='<?= $model->raza ?>'  data-incidentes='<?= $model->incidentes ?>' data-castrado='<?= $model->castrado ?>'  data-chip='<?= $model->chip ?>'>
                                    Cartilla
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Cartilla</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                                 
                                                  <div class='corregirModal'>
                                                 <b>Raza: </b> <span id='data-raza'></span> 
                                                 </div>

                                                <div class='corregirModal'>
                                                      <b>Incidentes: </b> <span id='data-incidentes'></span> 
                                                </div>

                                              
                                              
                                               
                                                 <div class='corregirModal'>
                                               <?=  "  <b>Castrado: </b> <span id='data-castrado'></span> " ?>
                                                 </div>


                            
                                               <div class='corregirModal'>
                                             <?=  " <b>Chip: </b><span id='data-chip'>   </span>" ?>
                                                 </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn colorboton " data-dismiss="modal">Cerrar</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>              
                    </div>     

                <?php }]); ?>
        </div> 
    </div>
</div>