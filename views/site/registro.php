<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\usuarios */
/* @var $form ActiveForm */
?>
<div class="site-registro">

    <?php $form = ActiveForm::begin(); ?>

    
        <?= $form->field($model, 'nombre')->textInput() ?>
        <?= $form->field($model, 'apellidos')->textInput() ?>
        <?= $form->field($model, 'direccion') ?>
        <?= $form->field($model, 'codigo_postal') ?>
        
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-registro -->
