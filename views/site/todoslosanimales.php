<?php

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'Animales';
?>
<div class ="pt-5 "></div>
<h1 class="display-4 titulo" style="padding-top: 15px">Todos los animales</h1>
<div class="site-index ">

    <div class="body-content solucioncontent ">

        <div class="pt-5"> </div>
        <div class="row">   

            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}",
                'itemView' => function($model) {
                    ?>

                    <div class=" col-md-6"> 
                        <div class="row  polaroid">
                            <div class="  col-6 alturaminima2 margenesimagen">

                                <?php
                                if ($model->tipo_de_animal == 'Gato' || 'gato') {

                                    echo Html::img('@web/imagenes/' . $model->raza . '.png', ['width' => '100%', 'height' => '100%'], ['alt' => 'My logo'], ['class' => 'card-img-bottom']);
                                } elseif ($model->tipo_de_animal == 'Perro' || 'perro') {

                                    echo Html::img('@web/imagenes/' . $model->raza . '.png', ['width' => '100%', 'height' => '100%'], ['alt' => 'My logo'], ['class' => 'card-img-bottom']);
                                }
                                ?>

                            </div>
                            <!--                         <div class="card col-6 ">-->
                            <div class=" col-5  centrartarjeta margenestexto ">
                                <h5 class="card-title"><b>Datos de interés</b></h5>

                                <div>



                                </div>
                                <div>
                                    <?= "<b>Nombre:</b> " . $model->nombre_mascota ?>

                                </div>

                                <div>
                                    <?= "<b>Animal:</b> " . $model->tipo_de_animal ?>

                                </div>
                                <div>
                                    <?= "<b>Edad: </b>" . $model->edad . " años" ?>
                                </div>

                                <?php
                                if (intval($model['adopcion']) == 0) {
                                    $posibilidad = 'No';
                                } else {
                                    $posibilidad = 'Sí';
                                }
                                ?>
                                <div>
                                    <?= "<b>Primera adopción:  </b>" . $posibilidad ?>
                                    <!--                            "Ha sido adoptado alguna vez: " . $model->adopcion -->

                                </div>

                                <br>

                                <?= Html::a('Adoptar', ['/formulario/create', 'animal' => $model->codigo_animal], ['class' => 'btn btn colorboton']) ?>
                                <!--                            
                                
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn colorboton mprueba" data-toggle="modal" data-target="#exampleModalCenter" data-raza='<?= $model->raza ?>'  data-incidentes='<?= $model->incidentes ?>' data-castrado='<?= $model->castrado ?>'  data-chip='<?= $model->chip ?>'>
                                    Cartilla
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Cartilla</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body ">


                                                <!--                                               <div class='corregirModal'>
                                                                                                   "  <b>Raza: </b> <span id='data-raza'></span>   " 
                                                                                              </div>-->
                                                <div class='corregirModal'>
                                                    <b>Raza: </b> <span id='data-raza'></span> 
                                                </div>

                                                <div class='corregirModal'>
                                                    <b>Incidentes: </b> <span id='data-incidentes'></span> 
                                                </div>




                                                <div class='corregirModal'>
                                                    <?= "  <b>Castrado: </b> <span id='data-castrado'></span> " ?>
                                                </div>



                                                <div class='corregirModal'>
                                                    <?= " <b>Chip: </b><span id='data-chip'>   </span>" ?>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn colorboton " data-dismiss="modal">Cerrar</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>

                        </div>              



                    </div> 


                <?php }]); ?>
        </div>
    </div>

</div> 