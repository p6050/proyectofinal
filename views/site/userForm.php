<?php

use yii\helpers\html;
use yii\widgets\ActiveForm;

?>

<?php
    if(yii::$app->session->hasFlash('success')){
        
        echo yii::$app->session->getFlash('success');
    }

?>

<div class="container">
 
    <div class="body-content solucioncontent ">
   
     <h1 class="display-4" style="padding-top: 60px">Rellena el formulario</h1>
     
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'name'); ?>
            <?= $form->field($model, 'apellidos'); ?>
            <?= $form->field($model, 'telefono'); ?>
            <?= $form->field($model, 'email'); ?>
            

           <?= Html::a('Descargar', ['/site/report'], ['class'=>'btn btn-primary']) ?>
<!--             Html::a($texto, [$link,'id'=>$model->id], ['class' => "btn ",'target'=>$target]);-->

    </div>  
</div>     


