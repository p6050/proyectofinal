<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'Protectoras';
?>  

<div class="container ">

    <div class="body-content">
        <div class="jumbotron text-center bg-transparent">
            <h1 class="display-4" style="padding-top: 15px">Elige una protectora</h1>
        </div>


        <div class="row">
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}",
                'itemView' => function($model) {
                    ?>


                    <div class="col-sm-6 col-md-4 "> 
                        <div class="card alturaminima  polaroid">

                            <?= Html::a(Html::img('@web/imagenes/' . $model->nombre_protectora . '.png', ['width' => '100%', 'height' => '100%']), ['site/listaanimales', 'codigo' => $model->codigo], ['class' => 'card-img-top center']) ?>

                            <div class='card-footer'>
                                <div class='row justify-content-center align-items-center'>
                                    <div class='col-6 align-self-center align-items-center  text-center'>
<!--                                        <button type="button" class="btn btn colorboton ">
                                            Ver animales
                                        </button>-->
                                          <?= Html::a('  Ver animales',  ['site/listaanimales','codigo'=>$model->codigo], ['class' => 'btn btn colorboton']) ?>
                                    </div>
                                    <!-- Button trigger modal -->
                                    <div class='col-6 align-self-center align-items-center  text-center'>
                                        <button type="button" class="btn btn colorboton  "  data-toggle="modal" data-target="#modal<?= $model->codigo?> ">
                                            Información
                                        </button>
                                    </div>
                                </div>


                                <!-- Modal -->
                                <div class="modal fade" id="modal<?= $model->codigo ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Información de la protectora</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p><?= "<b>Protectora: </b>" . $model->nombre_protectora ?></p>
                                                <p><?= "<b>Correo: </b>" . $model->email ?></p>
                                                <p><?= "<b>Teléfono: </b>" . $model->telefono ?></p>
                                                <p><?= "<b>Ciudad: </b>" . $model->ciudad ?></p>
                                                <p><?= "<b>Código postal: </b>" . $model->codigo_postal ?></p>
                                                <p><?= "<b>Dirección: </b>" . $model->direccion ?></p>
                                            
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn colorboton " data-dismiss="modal">cerrar</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>  


                <?php }]); ?>
        </div>
    </div> 
</div> 
