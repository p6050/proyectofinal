<?php

//use yii\widgets\ListView;

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Home';
?>





<div class="pt-5"> </div>

<h1 class ="titulo">¿Quiénes somos?</h1>

<div class="pt-4"> </div>

<p class="texto">Adopta & rescata somos una empresa sin ánimo de lucro, nuestro único objetivo es ayudar al máximo de protectoras posibles a que adopten a sus animalitos y también de alguna manera a las personas que estén interesados en adoptar o apadrinar a un animal.</p>
<p class="texto">En nuestra página podrás encontrar una variedad de protectoras con las que estamos trabajando.</p>

<h2 class ="titulo">¿Cuál es nuestro objetivo?</h2>
<div class="pt-4"> </div>

<p class="texto">Nuestro objetivo es trabajar con todas las protectoras posibles de diferentes regiones, facilitando de esa manera que cualquier persona de un vistazo encuentre todas las protectoras y de esa manera encontrar al acompañante perfecto en su vida. </p>

<div class="pt-5"> </div>


<div class="container-fluid">
    <div class="row stilobody">
        <div class=" col-sm-13 col-md-13 col-lg-13 ">

            <div class="size-carousel-flex polaroid " style="background-color: #d27979">

                <!--Carousel Wrapper-->
                <div id="carouselExampleIndicators" class="carousel slide " data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active "></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    

                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active" >
                            <div class="d-flex align-content-around" >
                                <div class="p-0">  <?= Html::a(Html::img('@web/imagenes/aspacancarrusel.png'), ['site/listaanimales', 'codigo' => '2'], ['class' => 'imageneshome']) ?></div>
    <!--                           <div class="align-self-center"> <p class="text-center centrarp">En Aspacan hay una agran variedad de perritos que les han abandonado esperando un hogar, Adopta un perrito. </p>  </div>-->
                                <div><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2898.5846266600142!2d-3.4160235842282307!3d43.40661057635836!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4eea9b8c61014d%3A0xc5f55cd8ac18b3dc!2sAspacan%20protectora%20y%20adopci%C3%B3n%20canina!5e0!3m2!1ses!2ses!4v1655135434633!5m2!1ses!2ses" width="500" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe></div>
                            </div>                      
                        </div>
                        <div class="carousel-item">
                             <div class="d-flex align-content-around" >
                                <div class="p-0"> <?= Html::a(Html::img('@web/imagenes/carruselT.png'), ['site/listaanimales', 'codigo' => '5'], ['class' => 'imageneshome']) ?></div>
                                <div><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d371337.38377261936!2d-4.604292050000042!3d43.35457710000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd49158cdfc9535b%3A0xf590322aa8dec7!2sRefugio%20Canino%20Torres%20Torrelavega!5e0!3m2!1ses!2ses!4v1655134647451!5m2!1ses!2ses" width="500" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe></div>
                            </div> 
                        </div>
                        <div class="carousel-item">
                           <div class="d-flex align-content-around" >
                                <div class="p-0"> <?= Html::a(Html::img('@web/imagenes/carruselAS.png'), ['site/listaanimales', 'codigo' => '6'], ['class' => 'imageneshome']) ?></div>
                                <div><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2895.326355979762!2d-3.852917784226719!3d43.4746591719749!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd49493335de3627%3A0x6e6c5b02b2d0684f!2sASPROAN%20Santander%20-%20Refugio%20Canino!5e0!3m2!1ses!2ses!4v1655135194633!5m2!1ses!2ses" width="500" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe></div>
     <!--                           <div class="align-self-center"> <p class="text-center centrarp">La asociación Perrucos Cantabria es una protectora que recoge todos los perros abandonados y busca a familias para que los perros puedan tener una mejor vida. </p>  </div>-->
                            </div>  
                        </div>
                       



                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon " aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon " aria-hidden="true"></span>
                        <span class="sr-only ">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

  