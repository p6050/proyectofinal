<?php
/* @var $this yii\web\View */
use yii\helpers\Html;



$this->title = 'Información';

?>
<div class="container">
    
    <div class="body-content">
        <div class="jumbotron text-center bg-transparent">
            <h1 class="display-4" style="padding-top: 15px">Datos generales</h1>
        </div>
        <div class="row">
            
            <div class="col-sm-6 col-md-4 "> 
                <div class="card alturacruds polaroid ">
                    
                    <div class="card-body tarjeta">
                        
                        <h3>Animales</h3>
                        <p></p>
                           
                            <?= Html::a(Html::img('@web/imagenes/animalesCRUDS.png',['width'=>'100%','height'=>'100%']),['/animales/index'],['class'=> 'imageneshome'])?>

                    </div>
                </div>
             </div>  
           
             <div class="col-sm-6 col-md-4"> 
                <div class="card alturacruds polaroid">
                    <div class="card-body tarjeta">
                        <h3>Animales apadrinados</h3>    
                        <p></p>
                        
                         <?= Html::a(Html::img('@web/imagenes/animalesAP.png',['width'=>'100%','height'=>'100%']),['/apadrinan/index'],['class'=> 'imageneshome'])?>
                          
                    </div>
                </div>
             </div>  
            
            <div class="col-sm-6 col-md-4"> 
                <div class="card alturacruds polaroid">
                    <div class="card-body tarjeta">
                        <h3>Cartillas de los animales</h3>
                        <p></p>                        
                        <?= Html::a(Html::img('@web/imagenes/cartillaP.png',['width'=>'100%','height'=>'100%']),['/cartillas/index'],['class'=> 'imageneshome']) ?>
                        
                    </div>
                </div>
             </div>    
         
            <div class="col-sm-6 col-md-4"> 
                <div class="card alturacruds polaroid">
                    
                    <div class="card-body tarjeta">
                        
                        <h3>Clientes</h3>
                        <p></p>
                        
                         <?= Html::a(Html::img('@web/imagenes/clientes.png',['width'=>'100%','height'=>'100%']),['/clientes/index'],['class'=> 'imageneshome'])?>
                        
                    </div>
                </div>
             </div>  
            
             <div class="col-sm-6 col-md-4"> 
                <div class="card alturacruds polaroid">
                    <div class="card-body tarjeta">
                        <h3>Protectoras</h3>    
                        <p></p>
                        
                         <?= Html::a(Html::img('@web/imagenes/iconoProtectora.png',['width'=>'100%','height'=>'100%']),['/protectoras/index'],['class'=> 'imageneshome'])?>
                        
                    </div>
                </div>
             </div>  
            
            <div class="col-sm-6 col-md-4"> 
                <div class="card alturacruds polaroid">
                    <div class="card-body tarjeta">
                        <h3>Animales revisados por los veterinarios</h3>
                        <p></p>
                        
                         <?= Html::a(Html::img('@web/imagenes/animalesRevisados.png',['width'=>'100%','height'=>'100%']),['/revisan/index'],['class'=> 'imageneshome'])?>
                        
                    </div>
                </div>
            </div> 
            
                 <div class="col-sm-6 col-md-4"> 
                <div class="card alturacruds polaroid">
                    
                    <div class="card-body tarjeta">
                        
                        <h3>Veterinario que trabaja en cada protectora</h3>
                        <p></p>
                        
                         <?= Html::a(Html::img('@web/imagenes/animalesdeP.png',['width'=>'100%','height'=>'100%']),['/tienen/index'],['class'=> 'imageneshome'])?>
                        
                    </div>
                </div>
             </div>  
            
             <div class="col-sm-6 col-md-4"> 
                <div class="card alturacruds polaroid">
                    <div class="card-body tarjeta">
                        <h3>Vacunas</h3>    
                        <p></p>
                        
                         <?= Html::a(Html::img('@web/imagenes/vacunasP.png',['width'=>'100%','height'=>'100%']),['/vacunas/index'],['class'=> 'imageneshome'])?>
                        
                    </div>
                </div>
             </div>  
            
            <div class="col-sm-6 col-md-4"> 
                <div class="card alturacruds polaroid">
                    <div class="card-body tarjeta">
                        <h3>Veterinarios</h3>
                        <p></p>
                        
                         <?= Html::a(Html::img('@web/imagenes/veterinarios.png',['width'=>'100%','height'=>'100%']),['/veterinarios/index'],['class'=> 'imageneshome'])?>
                        
                    </div>
                </div>
            </div> 
            
    </div>
    </div> 
</div> 
