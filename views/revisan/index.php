<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Animales revisados por los veterinarios';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pt-5"></div>
<div class="revisan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear revisión', ['create'], ['class' => 'btn btn colorboton']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'codigo_veterinario',
//            'codigo_animal',
              [
                'attribute'=>'Nombre de la mascota',
                'value'=>'codigoAnimal.nombre_mascota',
                ],
         
            [
                'attribute'=>'nombre del veterinario',
                'value'=>'codigoVeterinario.nombre',
                ],
             [
                'attribute'=>'numero de veterinario',
                'value'=>'codigoVeterinario.numero_profesional_veterinario',
                ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action,  $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
