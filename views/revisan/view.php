<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Revisan */

$this->title = $model->id;
//$this->params['breadcrumbs'][] = ['label' => 'Revisans', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//\yii\web\YiiAsset::register($this);
?>
<div class="pt-5"></div>
<div class="revisan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
         
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de que quieres eliminar esta revisión?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                 'attribute'=>'codigo_veterinario',
             'value'=>function($data){
            if(intval($data['codigo_veterinario'])==1){
                $cod="Mario";
            }
            else if(intval($data['codigo_veterinario'])==2){
                $cod="Ramón";
            }else if(intval($data['codigo_veterinario'])==3){
                $cod="Sara";
            }else if(intval($data['codigo_veterinario'])==4){
                $cod="Teresa";
            }else if(intval($data['codigo_veterinario'])==5){
                $cod="Juan David";
            }
            else if(intval($data['codigo_veterinario'])==6){
                $cod="Álvaro";
            }
            return $cod;
            }
        ],
            'codigo_animal',
        ],
    ]) ?>

</div>
