<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Revisan */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="revisan-form">

    <?php $form = ActiveForm::begin(); ?>

   
    <?= $form->field($model, 'codigo_veterinario')->textInput() ?>

    <?= $form->field($model, 'codigo_animal')->textInput() ?>

    <div class="form-group">
       <?= Html::submitButton('Guardar', ['class' => 'btn btn colorboton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
