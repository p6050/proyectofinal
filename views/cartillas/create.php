<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\cartillas */

$this->title = 'Crear Cartillas';
//$this->params['breadcrumbs'][] = ['label' => 'Cartillas', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pt-5"></div>
<div class="cartillas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
