<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\cartillas */

$this->title = $model->codigo;
//$this->params['breadcrumbs'][] = ['label' => 'Cartillas', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pt-5"></div>
<div class="cartillas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear vacunas', ['vacunas/create '], ['class' => 'btn btn colorboton']) ?>
        <?= Html::a('Actualizar', ['update', 'codigo' => $model->codigo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'codigo' => $model->codigo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de que quieres eliminar esta cartilla?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'codigo',
            'codigo_animal',
              [
                'attribute' => 'castrado',
                'value' => function($data) {
                    if (intval($data['castrado']) == 1) {
                        $cambio = "Sí";
                    } else {
                        $cambio = "No";
                    }
                    return $cambio;
                }
            ],
            'incidentes',
             [
                'attribute' => 'chip',
                'value' => function($data) {
                    if (intval($data['chip']) == 1) {
                        $cambio = "Sí";
                    } else {
                        $cambio = "No";
                    }
                    return $cambio;
                }
            ],
        ],
    ]) ?>

</div>
