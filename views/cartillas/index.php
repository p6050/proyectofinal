<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cartillas de los animales';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pt-5"></div>
<div class="cartillas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Cartilla', ['create'], ['class' => 'btn btn colorboton']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'codigo',
//            'codigo_animal',
             [
                'attribute'=>'Animal adoptado',
                'value'=>'codigoAnimal.nombre_mascota',
                ],
               [
             'attribute'=>'castrado',
             'value'=>function($data){
            if(intval($data['castrado'])==1){
                $cambio="Sí";
            }
            else{
                $cambio="No";
            }
            return $cambio;
            }
            ],
           
            'incidentes',
            [
             'attribute'=>'chip',
             'value'=>function($data){
            if(intval($data['chip'])==1){
                $cambio="Sí";
            }
            else{
                $cambio="No";
            }
            return $cambio;
            }
            ],
             
                 [
                'attribute'=>'raza',
                'value'=>'codigoAnimal.raza',
                ],
               
            
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action,  $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo' => $model->codigo]);
                 }
            ],
        ],
    ]); ?>


</div>
