<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\cartillas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cartillas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_animal')->textInput() ?>

    <?= $form->field($model, 'incidentes')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'castrado')->checkbox() ?>

    <?= $form->field($model, 'chip')->checkbox() ?>
    
    
     <div class='lineavertical'><p>marca la casilla para que sea si o desmarcala para no.</p> </div>
      <div class="pt-3"></div>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn colorboton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
