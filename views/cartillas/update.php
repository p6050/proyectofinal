<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\cartillas */

$this->title = 'Actualizar Cartillas: ' . $model->codigo;
//$this->params['breadcrumbs'][] = ['label' => 'Cartillas', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'codigo' => $model->codigo]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pt-5"></div>
<div class="cartillas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
