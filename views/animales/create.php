<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Animales */

$this->title = 'Crear Animales';
//$this->params['breadcrumbs'][] = ['label' => 'Animales', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pt-5"></div>
<div class="animales-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'protectoras'=>$protectoras,
      
    ]) ?>

</div>
