<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Animales';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pt-5"></div>
<div class="animales-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Animal', ['create'], ['class' => 'btn btn colorboton']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'codigo_animal',
//            'codigo',
//            'codigo_cliente',
            'nombre_mascota',
             [
             'attribute'=>'adopcion',
             'value'=>function($data){
            if(intval($data['adopcion'])==1){
                $cambio="Sí";
            }
            else{
                $cambio="No";
            }
            return $cambio;
            }
            ],
              
            'tipo_de_animal',
            [
             'attribute'=>'apto_para_adoptar',
             'value'=>function($data){
            if(intval($data['apto_para_adoptar'])==1){
                $cambio="Sí";
            }
            else{
                $cambio="No";
            }
            return $cambio;
            }
            ],
         
            'edad',
            'raza',
            
             
           
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action,  $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_animal' => $model->codigo_animal]);
                 }
            ]
                    
                    
        ],
    ]); ?>


</div>
