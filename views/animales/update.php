<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Animales */

$this->title = 'Actualizar Animales: ' . $model->codigo_animal;
//$this->params['breadcrumbs'][] = ['label' => 'Animales', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->codigo_animal, 'url' => ['view', 'codigo_animal' => $model->codigo_animal]];
//$this->params['breadcrumbs'][] = 'Actualizar  ';
?>
<div class="pt-5"></div>
<div class="animales-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
         'protectoras'=>$protectoras,
        
    ]) ?>

</div>
