<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Animales */

$this->title = $model->codigo_animal;
//$this->params['breadcrumbs'][] = ['label' => 'Animales', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pt-5"></div>
<div class="animales-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear cartilla', ['cartillas/create '], ['class' => 'btn btn colorboton']) ?>
        <?= Html::a('Actualizar', ['update', 'codigo_animal' => $model->codigo_animal], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Borrar', ['delete', 'codigo_animal' => $model->codigo_animal], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de que quieres eliminar este animal?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [[
                 'attribute'=>'codigo',
             'value'=>function($data){
            if(intval($data['codigo'])==1){
                $protecto="Amigat";
            }
            else if(intval($data['codigo'])==2){
                $protecto="Asproan";
            }else if(intval($data['codigo'])==3){
                $protecto="Galgos & podencos Cantabria";
            }else if(intval($data['codigo'])==4){
                $protecto="Perrucos Cantabria";
            }else if(intval($data['codigo'])==5){
                $protecto="Refugio Canino Torres";
            }
            else if(intval($data['codigo'])==6){
                $protecto="Aspascan";
            }
            return $protecto;
            }
        ],
           
            'nombre_mascota',
            [
                'attribute' => 'adopcion',
                'value' => function($data) {
                    if (intval($data['adopcion']) == 1) {
                        $cambio = "Sí";
                    } else {
                        $cambio = "No";
                    }
                    return $cambio;
                }
            ],
            'tipo_de_animal',
            [
                'attribute' => 'apto_para_adoptar',
                'value' => function($data) {
                    if (intval($data['apto_para_adoptar']) == 1) {
                        $cambio = "Sí";
                    } else {
                        $cambio = "No";
                    }
                    return $cambio;
                }
            ],
            'edad',
            'raza',
        ],
    ])
    ?>

</div>
