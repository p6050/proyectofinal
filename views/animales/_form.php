<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Animales */
/* @var $form yii\widgets\ActiveForm */

 $protectora = ArrayHelper::map($protectoras, 'codigo', 'nombre_protectora');

?>

<div class="animales-form">
   

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model,'codigo')->dropDownList($protectora,['class'=>'form-control','prompt'=>'Seleccione la protectora','id'=>'protectora']) ?>
    
  <!--$form->field($model, 'codigo_cliente')->textInput()--> 

    <?= $form->field($model, 'nombre_mascota')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model,'tipo_de_animal')->textInput(['maxlength' => true]) ?>
<!--$form->field($model, 'tipo_de_animal')->textInput(['maxlength' => true]) -->


    <?= $form->field($model, 'edad')->textInput() ?>

    <?= $form->field($model, 'raza')->textInput(['maxlength' => true]) ?>

    <div class='lineavertical'><p>marca la casilla para que sea si o desmarcala para no.</p> </div>
 <div class="pt-3"></div>

    <?= $form->field($model, 'adopcion')->checkbox() ?>
  
   <?= $form->field($model, 'apto_para_adoptar')->checkbox() ?>


       <div class="pt-3"></div>
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn colorboton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
