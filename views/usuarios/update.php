<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\usuarios */

$this->title = 'Update Usuarios: ' . $model->codigo_usuario;
//$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->codigo_usuario, 'url' => ['view', 'codigo_usuario' => $model->codigo_usuario]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pt-5"></div>
<div class="usuarios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
