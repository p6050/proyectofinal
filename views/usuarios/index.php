<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pt-5"></div>
<div class="usuarios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Usuarios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_usuario',
            'nombre',
            'apellidos',
            'direccion',
            'codigo_postal',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, usuarios $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_usuario' => $model->codigo_usuario]);
                 }
            ],
        ],
    ]); ?>


</div>
