<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Protectoras */

$this->title = 'Crear Protectoras';
//$this->params['breadcrumbs'][] = ['label' => 'Protectoras', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pt-5"></div>
<div class="protectoras-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
