<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "animales".
 *
 * @property int $codigo_animal
 * @property int|null $codigo
 * @property int|null $codigo_cliente
 * @property string|null $nombre_mascota
 * @property int|null $adopcion
 * @property string|null $tipo_de_animal
 * @property int|null $apto_para_adoptar
 * @property int|null $edad
 * @property string|null $raza
 *
 * @property Apadrinan[] $apadrinans
 * @property Cartillas[] $cartillas
 * @property Protectoras $codigo0
 * @property Clientes $codigoCliente
 * @property Clientes[] $codigoClientes
 * @property Veterinarios[] $codigoVeterinarios
 * @property Revisan[] $revisans
 */
class Animales extends \yii\db\ActiveRecord
{
    
   public $nombre_protectora;
   public $chip;
   public $incidentes;
   public $castrado;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
         
        return 'animales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'edad'], 'integer'],
            [['codigo',  'edad'], 'required'],
            [['edad'],'match','pattern'=>'(^[0-9]{1,2}$|^[0-9]{1,2}\.[0-9]{1,2}$)'],
            [['adopcion', 'apto_para_adoptar'],'required'], 
            [['nombre_mascota'],'match', 'pattern' => '/^[a-zA-ZÀ-ÿ\s]{0,60}$/','message' =>'No se acepta numeros'],
            [['nombre_mascota', 'tipo_de_animal'], 'string', 'max' => 30],
            [['nombre_mascota', 'tipo_de_animal'], 'required'],
            [['raza'], 'string', 'max' => 20],
            [['raza'], 'required'],
//            [['codigo_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['codigo_cliente' => 'codigo_cliente']],
            [['codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Protectoras::className(), 'targetAttribute' => ['codigo' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_animal' => 'Codigo animal',
            'codigo' => 'Protectora',
            'codigo_cliente' => 'Codigo cliente',
            'nombre_mascota' => 'Nombre mascota',
            'adopcion' => 'Adopción',
            'tipo_de_animal' => 'Tipo de animal',
            'apto_para_adoptar' => 'Apto para adoptar',
            'edad' => 'Edad',
            'raza' => 'Raza',
        ];
    }

    /**
     * Gets query for [[Apadrinans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApadrinans()
    {
        return $this->hasMany(Apadrinan::className(), ['codigo_animal' => 'codigo_animal']);
    }

    /**
     * Gets query for [[Cartillas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCartillas()
    {
        return $this->hasMany(Cartillas::className(), ['codigo_animal' => 'codigo_animal']);
    }

    /**
     * Gets query for [[Codigo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigo0()
    {
        return $this->hasOne(Protectoras::className(), ['codigo' => 'codigo']);
    }

    /**
     * Gets query for [[CodigoCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCliente()
    {
        return $this->hasOne(Clientes::className(), ['codigo_cliente' => 'codigo_cliente']);
    }

    /**
     * Gets query for [[CodigoClientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoClientes()
    {
        return $this->hasMany(Clientes::className(), ['codigo_cliente' => 'codigo_cliente'])->viaTable('apadrinan', ['codigo_animal' => 'codigo_animal']);
    }

    /**
     * Gets query for [[CodigoVeterinarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoVeterinarios()
    {
        return $this->hasMany(Veterinarios::className(), ['codigo_veterinario' => 'codigo_veterinario'])->viaTable('revisan', ['codigo_animal' => 'codigo_animal']);
    }

    /**
     * Gets query for [[Revisans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRevisans()
    {
        return $this->hasMany(Revisan::className(), ['codigo_animal' => 'codigo_animal']);
    }
}
