<?php

namespace app\models;

use Yii;



/**
 * This is the model class for table "clientes".
 *
 * @property int $codigo_cliente
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property int|null $telefono
 * @property string|null $email
 * @property string|null $direccion
 * @property string|null $dni
 *
 * @property Animales[] $animales
 * @property Apadrinan[] $apadrinans
 * @property Animales[] $codigoAnimals
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      
        return [
            [['telefono'], 'integer', 'max' => 9],
            [['telefono'], 'required'],
            [['telefono'],'match', 'pattern' =>'/^6[0-9]{8}$/','message'=>'Solo se pueden 9 números'],
            [['nombre', 'apellidos'],'match', 'pattern' => '/^[a-zA-ZÀ-ÿ\s]{0,60}$/','message' =>'No se acepta numeros'],
            [['nombre', 'apellidos', 'email', 'direccion'], 'string', 'max' => 30],
            [['nombre', 'apellidos', 'email', 'direccion'],'required'],
            [['email'],'email'],
            [['dni'],'string', 'max' => 9],
            
            
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_cliente' => 'Codigo Cliente',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'telefono' => 'Telefono',
            'email' => 'Email',
            'direccion' => 'Direccion',
            'dni' => 'Dni',
        ];
    }

    /**
     * Gets query for [[Animales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnimales()
    {
        return $this->hasMany(Animales::className(), ['codigo_cliente' => 'codigo_cliente']);
    }

    /**
     * Gets query for [[Apadrinans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApadrinans()
    {
        return $this->hasMany(Apadrinan::className(), ['codigo_cliente' => 'codigo_cliente']);
    }

    /**
     * Gets query for [[CodigoAnimals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAnimals()
    {
        return $this->hasMany(Animales::className(), ['codigo_animal' => 'codigo_animal'])->viaTable('apadrinan', ['codigo_cliente' => 'codigo_cliente']);
    }
}
