<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacunas".
 *
 * @property int $id
 * @property int|null $codigo
 * @property string|null $vacunas
 *
 * @property Cartillas $codigo0
 */
class Vacunas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacunas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'integer'],
           [['codigo'], 'unique'],
            [['codigo'], 'required'],
            [['vacunas'], 'string', 'max' => 50],
            [['vacunas'], 'required'],
            [['vacunas'], 'match', 'pattern' => '/^[a-zA-ZÀ-ÿ\s]{0,60}$/','message' =>'No se acepta numeros'],
            [['codigo', 'vacunas'], 'unique', 'targetAttribute' => ['codigo', 'vacunas']],
            [['codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Cartillas::className(), 'targetAttribute' => ['codigo' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Código',
            'vacunas' => 'Vacunas',
        ];
    }

    /**
     * Gets query for [[Codigo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigo0()
    {
        return $this->hasOne(Cartillas::className(), ['codigo' => 'codigo']);
    }
}
