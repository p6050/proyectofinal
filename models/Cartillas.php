<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cartillas".
 *
 * @property int $codigo
 * @property int|null $codigo_animal
 * @property int|null $castrado
 * @property string|null $incidentes
 * @property int|null $chip
 *
 * @property Animales $codigoAnimal
 * @property Vacunas[] $vacunas
 */
class Cartillas extends \yii\db\ActiveRecord
{
  
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cartillas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_animal', 'castrado', 'chip'], 'integer'],
            [['codigo_animal', 'castrado', 'chip'], 'required'],
            [['codigo_animal'],'unique'],
            [['incidentes'], 'string', 'max' => 300],
            [['incidentes'], 'required' ],
            [['codigo_animal'], 'exist', 'skipOnError' => true, 'targetClass' => Animales::className(), 'targetAttribute' => ['codigo_animal' => 'codigo_animal']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'codigo_animal' => 'Codigo Animal',
            'castrado' => 'Castrado',
            'incidentes' => 'Incidentes',
            'chip' => 'Chip',
        ];
    }

    /**
     * Gets query for [[CodigoAnimal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAnimal()
    {
        return $this->hasOne(Animales::className(), ['codigo_animal' => 'codigo_animal']);
    }

    /**
     * Gets query for [[Vacunas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVacunas()
    {
        return $this->hasMany(Vacunas::className(), ['codigo' => 'codigo']);
    }
}
