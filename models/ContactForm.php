<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'código de verificación',
            'name' => 'Nombre',
            'body' => 'Introducza su sugerencia',
            'subject' => 'Asunto',
           
            
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        $content = "<p>Email: " . $this->email . "</p>";
        $content .= "<p>Name: " . $this->name . "</p>";
        $content .= "<p>Subject: " . $this->subject . "</p>";
        $content .= "<p>Body: " . $this->body . "</p>";
        

        
        if ($this->validate()) {
       Yii::$app->mailer->compose()
            ->setFrom(["ivaanmarin25@gmail.com" => "My Name"])
            ->setTo(['byivan19@hotmail.com' => 'Iván'])
            ->setSubject("The Subject")
            ->setHtmlBody("<html><body><h1>Hello</h1></body></html>")
            ->send();

            return true;
        }
        return false;
    }
}
