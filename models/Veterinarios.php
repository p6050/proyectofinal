<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "veterinarios".
 *
 * @property int $codigo_veterinario
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property int|null $numero_profesional_veterinario
 *
 * @property Animales[] $codigoAnimals
 * @property Protectoras[] $codigoProtectoras
 * @property Revisan[] $revisans
 * @property Tienen[] $tienens
 */
class Veterinarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'veterinarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero_profesional_veterinario'], 'integer'],
            [['numero_profesional_veterinario'], 'required'],
            [['nombre', 'apellidos'], 'string', 'max' => 30],
            [['nombre', 'apellidos'], 'required'],
            [['nombre', 'apellidos'],'match', 'pattern' => '/^[a-zA-ZÀ-ÿ\s]{0,60}$/','message' =>'No se acepta numeros'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_veterinario' => 'Codigo Veterinario',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'numero_profesional_veterinario' => 'Numero Profesional Veterinario',
        ];
    }

    /**
     * Gets query for [[CodigoAnimals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAnimals()
    {
        return $this->hasMany(Animales::className(), ['codigo_animal' => 'codigo_animal'])->viaTable('revisan', ['codigo_veterinario' => 'codigo_veterinario']);
    }

    /**
     * Gets query for [[CodigoProtectoras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProtectoras()
    {
        return $this->hasMany(Protectoras::className(), ['codigo' => 'codigo_protectora'])->viaTable('tienen', ['codigo_veterinario' => 'codigo_veterinario']);
    }

    /**
     * Gets query for [[Revisans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRevisans()
    {
        return $this->hasMany(Revisan::className(), ['codigo_veterinario' => 'codigo_veterinario']);
    }

    /**
     * Gets query for [[Tienens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienens()
    {
        return $this->hasMany(Tienen::className(), ['codigo_veterinario' => 'codigo_veterinario']);
    }
}
