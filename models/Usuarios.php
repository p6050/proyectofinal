<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $codigo_usuario
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property int|null $direccion
 * @property int|null $codigo_postal
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['direccion', 'codigo_postal'], 'integer'],
             [['direccion', 'codigo_postal'], 'required'],
            [['nombre', 'apellidos'], 'string', 'max' => 30],
            [['nombre', 'apellidos'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_usuario' => 'Codigo Usuario',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'direccion' => 'Direccion',
            'codigo_postal' => 'Codigo Postal',
        ];
    }
}
