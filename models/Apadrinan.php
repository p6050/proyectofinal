<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apadrinan".
 *
 * @property int $id
 * @property int|null $codigo_animal
 * @property int|null $codigo_cliente
 *
 * @property Animales $codigoAnimal
 * @property Clientes $codigoCliente
 */
class Apadrinan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apadrinan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_animal', 'codigo_cliente'], 'integer'],
            [['codigo_animal', 'codigo_cliente'], 'unique', 'targetAttribute' => ['codigo_animal', 'codigo_cliente']],
            [['codigo_animal'], 'exist', 'skipOnError' => true, 'targetClass' => Animales::className(), 'targetAttribute' => ['codigo_animal' => 'codigo_animal']],
            [['codigo_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['codigo_cliente' => 'codigo_cliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_animal' => 'Codigo Animal',
            'codigo_cliente' => 'Codigo Cliente',
        ];
    }

    /**
     * Gets query for [[CodigoAnimal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAnimal()
    {
        return $this->hasOne(Animales::className(), ['codigo_animal' => 'codigo_animal']);
    }

    /**
     * Gets query for [[CodigoCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCliente()
    {
        return $this->hasOne(Clientes::className(), ['codigo_cliente' => 'codigo_cliente']);
    }
}
