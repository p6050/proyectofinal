<?php

namespace app\models;
use Yii;
use Egulias\EmailValidator\Result\ValidEmail;


/**
 * This is the model class for table "formulario".
 *
 * @property int $id
 * @property int|null $codigo_animal
 * @property string|null $nombre_usuario
 * @property string|null $apellido_usuario
 * @property int|null $telefono_usuario
 * @property string|null $correo
 *
 * @property Animales $codigoAnimal
 */
class Formulario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'formulario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        
   
    

        return [
            [['codigo_animal', 'telefono_usuario'], 'integer'],     
            [['nombre_usuario', 'apellido_usuario'], 'string', 'max' => 60],    
            [['nombre_usuario','apellido_usuario'], 'required'],
            [['nombre_usuario','apellido_usuario'], 'trim'],
            [['nombre_usuario','apellido_usuario'],'match', 'pattern' => '/^[a-zA-ZÀ-ÿ\s]{0,60}$/','message' =>'No se acepta numeros'],
            [['telefono_usuario'],'match', 'pattern' =>'/^6[0-9]{8}$/','message'=>'Solo se pueden 9 números'],
            [['telefono_usuario'], 'trim'],
            [['telefono_usuario'],'required'],
            [['correo'], 'string', 'max' => 30],
            [['correo'], 'required'],
            [['correo'], 'email'],
            [['codigo_animal'], 'exist', 'skipOnError' => true, 'targetClass' => Animales::className(), 'targetAttribute' => ['codigo_animal' => 'codigo_animal']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_animal' => 'Codigo Animal',
            'nombre_usuario' => 'Nombre ',
            'apellido_usuario' => 'Apellido ',
            'telefono_usuario' => 'Telefono ',
            'correo' => 'Correo',
        ];
    }

    /**
     * Gets query for [[CodigoAnimal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAnimal()
    {
        return $this->hasOne(Animales::className(), ['codigo_animal' => 'codigo_animal']);
    }
}
