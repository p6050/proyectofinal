<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "protectoras".
 *
 * @property int $codigo
 * @property string|null $nombre_protectora
 * @property string|null $email
 * @property int|null $telefono
 * @property string|null $ciudad
 * @property int|null $codigo_postal
 * @property string|null $direccion
 *
 * @property Animales[] $animales
 * @property Veterinarios[] $codigoVeterinarios
 * @property Tienen[] $tienens
 */
class Protectoras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'protectoras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telefono', 'codigo_postal'], 'required'],
            [['telefono', 'codigo_postal'], 'integer'],
            [['telefono'],'match', 'pattern' =>'/^6[0-9]{8}$/','message'=>'Solo se pueden 9 números'],
            [['telefono'], 'required'],
            [['nombre_protectora'],'match', 'pattern' => '/^[a-zA-ZÀ-ÿ\s]{0,60}$/','message' =>'No se acepta numeros'],
            [['nombre_protectora'], 'string', 'max' => 60],
            [['nombre_protectora'], 'required'],
            [['email'],'email'],
            [['email'],'required'],
            [['email', 'ciudad', 'direccion'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre_protectora' => 'Nombre Protectora',
            'email' => 'Email',
            'telefono' => 'Telefono',
            'ciudad' => 'Ciudad',
            'codigo_postal' => 'Codigo Postal',
            'direccion' => 'Direccion',
        ];
    }

    /**
     * Gets query for [[Animales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnimales()
    {
        return $this->hasMany(Animales::className(), ['codigo' => 'codigo']);
    }
   
    /**
     * Gets query for [[CodigoVeterinarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoVeterinarios()
    {
        return $this->hasMany(Veterinarios::className(), ['codigo_veterinario' => 'codigo_veterinario'])->viaTable('tienen', ['codigo_protectora' => 'codigo']);
    }

    /**
     * Gets query for [[Tienens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienens()
    {
        return $this->hasMany(Tienen::className(), ['codigo_protectora' => 'codigo']);
    }
}
