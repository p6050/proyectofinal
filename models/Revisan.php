<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "revisan".
 *
 * @property int $id
 * @property int|null $codigo_veterinario
 * @property int|null $codigo_animal
 *
 * @property Animales $codigoAnimal
 * @property Veterinarios $codigoVeterinario
 */
class Revisan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'revisan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_veterinario', 'codigo_animal'], 'integer'],
             [['codigo_veterinario', 'codigo_animal'], 'required'],
            [['codigo_veterinario', 'codigo_animal'], 'unique', 'targetAttribute' => ['codigo_veterinario', 'codigo_animal']],
            [['codigo_animal'], 'exist', 'skipOnError' => true, 'targetClass' => Animales::className(), 'targetAttribute' => ['codigo_animal' => 'codigo_animal']],
            [['codigo_veterinario'], 'exist', 'skipOnError' => true, 'targetClass' => Veterinarios::className(), 'targetAttribute' => ['codigo_veterinario' => 'codigo_veterinario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_veterinario' => 'Codigo Veterinario',
            'codigo_animal' => 'Codigo Animal',
        ];
    }

    /**
     * Gets query for [[CodigoAnimal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAnimal()
    {
        return $this->hasOne(Animales::className(), ['codigo_animal' => 'codigo_animal']);
    }

    /**
     * Gets query for [[CodigoVeterinario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoVeterinario()
    {
        return $this->hasOne(Veterinarios::className(), ['codigo_veterinario' => 'codigo_veterinario']);
    }
}
