<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tienen".
 *
 * @property int $id
 * @property int|null $codigo_protectora
 * @property int|null $codigo_veterinario
 *
 * @property Protectoras $codigoProtectora
 * @property Veterinarios $codigoVeterinario
 */
class Tienen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tienen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_protectora', 'codigo_veterinario'], 'integer'],
             [['codigo_protectora', 'codigo_veterinario'], 'required'],
            [['codigo_protectora', 'codigo_veterinario'], 'unique', 'targetAttribute' => ['codigo_protectora', 'codigo_veterinario']],
            [['codigo_protectora'], 'exist', 'skipOnError' => true, 'targetClass' => Protectoras::className(), 'targetAttribute' => ['codigo_protectora' => 'codigo']],
            [['codigo_veterinario'], 'exist', 'skipOnError' => true, 'targetClass' => Veterinarios::className(), 'targetAttribute' => ['codigo_veterinario' => 'codigo_veterinario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_protectora' => 'Codigo Protectora',
            'codigo_veterinario' => 'Codigo Veterinario',
        ];
    }

    /**
     * Gets query for [[CodigoProtectora]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProtectora()
    {
        return $this->hasOne(Protectoras::className(), ['codigo' => 'codigo_protectora']);
    }

    /**
     * Gets query for [[CodigoVeterinario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoVeterinario()
    {
        return $this->hasOne(Veterinarios::className(), ['codigo_veterinario' => 'codigo_veterinario']);
    }
}
