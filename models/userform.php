<?php

namespace app\models;

use yii\base\Model;

class userform extends Model{
    
    public $name;
    public $email;
    public $telefono;
    public $apellidos;
    
    public function rules(){
      
        return [
            [['name','apellidos','telefono','email'],'required'],
            ['email','email']
        ];
        
    }
    
    public function attributeLabels(){
        
        
        return [
           'name'=>'Nombre',
           'apellidos'=>'Apellidos',
           'telefono'=>'Teléfono',
           'email'=>'Correo electrónico'
            
            
        ];
    }
    
}

//

