<?php

namespace app\controllers;

use app\models\Clientes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientesController implements the CRUD actions for Clientes model.
 */
class ClientesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Clientes models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Clientes::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_cliente' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Clientes model.
     * @param int $codigo_cliente Codigo Cliente
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_cliente)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_cliente),
        ]);
    }
     public function actionView2($codigo_cliente)
    {
        return $this->render('view2', [
            'model' => $this->findModel($codigo_cliente),
        ]);
    }

    /**
     * Creates a new Clientes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Clientes();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo_cliente' => $model->codigo_cliente]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
     public function actionCreate2()
    {
        $model = new Clientes();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view2', 'codigo_cliente' => $model->codigo_cliente]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create2', [
            'model' => $model,
        ]);
    }

  public function actionForm2($id){
        
        $model=$this->findModel($id);
        return $model;
        
    }
    
       public function actionReport($codigo_cliente){ 
        
        $model= $this->findModel($codigo_cliente);
        
        $protectora=\yii::$app->db->createCommand('SELECT concat(nombre_protectora,"<br>", telefono,"<br>", direccion) FROM protectoras where codigo = ' . $model->codigoAnimals->codigo )->queryScalar();
        $html = $this->renderPartial('/site/_reportView',['model'=>$model,'protectora' => $protectora]);
        
        $pdf = \yii::$app->pdf;
        $pdf->content=$html;
        return $pdf->render();
        
        
            
//            $content = $this->renderPartial('_ReportView');
//            $pdf = yii::$app->pdf;
//            
//            $mpdf = $pdf->api;
//            
//            $mpdf->Bookmark('inicio');
//            $mpdf-> writeHTML('<div>hola mundo</div>');
//            $mpdf->Output();
//            return $mpdf->render();
    }

    /**
     * Updates an existing Clientes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo_cliente Codigo Cliente
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_cliente)
    {
        $model = $this->findModel($codigo_cliente);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_cliente' => $model->codigo_cliente]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Clientes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo_cliente Codigo Cliente
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_cliente)
    {
        $this->findModel($codigo_cliente)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Clientes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo_cliente Codigo Cliente
     * @return Clientes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_cliente)
    {
        if (($model = Clientes::findOne(['codigo_cliente' => $codigo_cliente])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
