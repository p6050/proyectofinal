<?php

namespace app\controllers;


use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Protectoras;
use yii\data\ActiveDataProvider;
use app\models\Animales;
use app\models\userform;
use kartik\mpdf\Pdf;
use app\models\cartillas;
use app\models\formulario;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'=>  ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionProtectoras(){
        
       $dataProvider = new ActiveDataProvider([
            'query' => Protectoras::find(),
            
        ]);
        
        return $this->render('protectoras',[
        "dataProvider"=>$dataProvider , 
        ]);
    }
    public function actionCruds(){
        
        return $this->render('cruds');
    }
   
    public function actionUser(){
       
        $model = new userform;
                
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            
            Yii::$app->session->setFlash('success','Has introducido los datos correctamente');
            
        }
        return $this->render('userForm',['model'=>$model]);
    }
   
    
      ////////////////////////////////////////////////////////////////////////// FUNCIONES LISTA DE ANIMALES /////////////////////////////////////////////////////////////////////
    
    public function actionListaanimales($codigo){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Animales::find()
            ->select('animales.codigo_animal,protectoras.nombre_protectora,tipo_de_animal,edad,raza,cartillas.castrado,cartillas.chip,cartillas.incidentes,nombre_mascota,adopcion')
                                       ->innerjoin('protectoras','protectoras.codigo = animales.codigo')
                                       ->where('apto_para_adoptar = 1 and formulario.codigo_animal is null') 
                                       ->andwhere("protectoras.codigo= " . $codigo) 
                                       ->innerjoin('cartillas','cartillas.codigo_animal = animales.codigo_animal')
                                       ->leftJoin("formulario", $on="formulario.codigo_animal=animales.codigo_animal"),
                                       
            'pagination' => false,
            ]);
        return $this->render('listaanimales',[
         "dataProvider"=>$dataProvider , 
            
            
        ]);  
    }
//     public function actionListaanimales2(){
//        
//        $dataProvider = new ActiveDataProvider([
//            'query' => Animales::find()
//                 ->select('animales.codigo_animal,protectoras.nombre_protectora,tipo_de_animal,edad,raza,cartillas.castrado,cartillas.chip,cartillas.incidentes')
//                                       ->innerjoin('protectoras','protectoras.codigo = animales.codigo')
//                                       ->where('apto_para_adoptar = 1 and formulario.codigo_animal is null') 
//                                       ->andwhere("protectoras.codigo=2") 
//                                       ->innerjoin('cartillas','cartillas.codigo_animal = animales.codigo_animal')
//                                       ->leftJoin("formulario", $on="formulario.codigo_animal=animales.codigo_animal"),
//            'pagination' => false,
//            ]);
//        return $this->render('listaanimales',[
//         "dataProvider"=>$dataProvider , 
//            
//            
//        ]);  
//    }
//      public function actionListaanimales3(){
//        
//        $dataProvider = new ActiveDataProvider([
//            'query' => Animales::find()
//                 ->select('animales.codigo_animal,protectoras.nombre_protectora,tipo_de_animal,edad,raza,cartillas.castrado,cartillas.chip,cartillas.incidentes')
//                                       ->innerjoin('protectoras','protectoras.codigo = animales.codigo')
//                                       ->where('apto_para_adoptar = 1 and formulario.codigo_animal is null') 
//                                       ->andwhere("protectoras.codigo=3") 
//                                       ->innerjoin('cartillas','cartillas.codigo_animal = animales.codigo_animal')
//                                       ->leftJoin("formulario", $on="formulario.codigo_animal=animales.codigo_animal"),
//            'pagination' => false,
//            ]);
//        return $this->render('listaanimales',[
//         "dataProvider"=>$dataProvider , 
//            
//            
//        ]);  
//    }
//      public function actionListaanimales4(){
//        
//        $dataProvider = new ActiveDataProvider([
//            'query' => Animales::find()
//                 ->select('animales.codigo_animal,protectoras.nombre_protectora,tipo_de_animal,edad,raza,cartillas.castrado,cartillas.chip,cartillas.incidentes')
//                                       ->innerjoin('protectoras','protectoras.codigo = animales.codigo')
//                                       ->where('apto_para_adoptar = 1 and formulario.codigo_animal is null') 
//                                       ->andwhere("protectoras.codigo=4")
//                                       ->innerjoin('cartillas','cartillas.codigo_animal = animales.codigo_animal')
//                                       ->leftJoin("formulario", $on="formulario.codigo_animal=animales.codigo_animal"),
//            'pagination' => false,
//            ]);
//        return $this->render('listaanimales',[
//         "dataProvider"=>$dataProvider , 
//            
//            
//        ]);  
//    }
//      public function actionListaanimales5(){
//        
//        $dataProvider = new ActiveDataProvider([
//            'query' => Animales::find()
//                 ->select('animales.codigo_animal,protectoras.nombre_protectora,tipo_de_animal,edad,raza,cartillas.castrado,cartillas.chip,cartillas.incidentes')
//                                       ->innerjoin('protectoras','protectoras.codigo = animales.codigo')
//                                       ->where('apto_para_adoptar = 1 and formulario.codigo_animal is null') 
//                                       ->andwhere("protectoras.codigo=5") 
//                                       ->innerjoin('cartillas','cartillas.codigo_animal = animales.codigo_animal')
//                                       ->leftJoin("formulario", $on="formulario.codigo_animal=animales.codigo_animal"),
//            'pagination' => false,
//            ]);
//        return $this->render('listaanimales',[
//         "dataProvider"=>$dataProvider , 
//            
//            
//        ]);  
//    }
//      public function actionListaanimales6(){
//        
//        $dataProvider = new ActiveDataProvider([
//            'query' => Animales::find()
//                 ->select('animales.codigo_animal,protectoras.nombre_protectora,tipo_de_animal,edad,raza,cartillas.castrado,cartillas.chip,cartillas.incidentes')
//                                       ->innerjoin('protectoras','protectoras.codigo = animales.codigo')
//                                       ->where('apto_para_adoptar = 1 and formulario.codigo_animal is null') 
//                                       ->andwhere("protectoras.codigo=6")
//                                       ->innerjoin('cartillas','cartillas.codigo_animal = animales.codigo_animal')
//                                       ->leftJoin("formulario", $on="formulario.codigo_animal=animales.codigo_animal"),
//            'pagination' => false,
//            ]);
//        return $this->render('listaanimales',[
//         "dataProvider"=>$dataProvider , 
//            
//            
//        ]);  
//    }
//    
    
    ////////////////////////////////////////////////////////////////////////// FUNCIONES PAGINA ANIMALES APADRINADOS /////////////////////////////////////////////////////////////////////
     
     public function actionAnimalesapadrinados() {

        $dataProvider = new ActiveDataProvider([
            'query' => Animales::find()->select('animales.codigo_animal,nombre_protectora,nombre_mascota,apto_para_adoptar,edad,raza,cartillas.chip,cartillas.castrado,cartillas.incidentes')
                    ->innerjoin('cartillas', 'cartillas.codigo_animal = animales.codigo_animal')
                    ->where('apto_para_adoptar = 1 and formulario.codigo_animal is null')     
                    ->leftJoin("formulario", $on="formulario.codigo_animal=animales.codigo_animal")
                    ->innerjoin('protectoras','protectoras.codigo = animales.codigo')
                    ->innerjoin('apadrinan','apadrinan.codigo_animal = animales.codigo_animal'),
                                       
            'pagination' => false,
        ]);
        return $this->render('animalesApadrinados', [
                    "dataProvider" => $dataProvider,
        ]);
    }

//    SELECT  tipo_de_animal,edad,raza FROM apadrinan INNER JOIN animales a ON apadrinan.codigo_animal = a.codigo_animal WHERE tipo_de_animal = 1 AND adopcion = 1;
//    public function actionAnimalesApadrinados2(){
//        
//        
//        
//         return $this->render('animalesapadrinados');
//    }
//    
      ////////////////////////////////////////////////////////////////////////// FUNCIONES PAGINA ANIMALES DEVUELTOS /////////////////////////////////////////////////////////////////////
    
    public function actionAnimalesdevueltos(){
        
     $dataProvider = new ActiveDataProvider([
            'query' => Animales::find()->select('animales.codigo_animal,protectoras.nombre_protectora,nombre_mascota,tipo_de_animal,edad,raza,cartillas.chip,cartillas.castrado,cartillas.incidentes')
                                       ->innerjoin('protectoras','protectoras.codigo = animales.codigo')
                                       ->where('apto_para_adoptar = 1 and formulario.codigo_animal is null') 
                                       ->innerjoin('cartillas','cartillas.codigo_animal = animales.codigo_animal')
                                       ->leftJoin("formulario", $on="formulario.codigo_animal=animales.codigo_animal"),
                                       
                                       
            'pagination' => false,
            ]);
        return $this->render('animalesDevueltos',[
         "dataProvider"=>$dataProvider , 
        
        ]); 
    } 
       ////////////////////////////////////////////////////////////////////////// funcion pagina todos los animales /////////////////////////////////////////////////////////////////////
   public function actionTodoslosanimales(){
        
     $dataProvider = new ActiveDataProvider([
            'query' => Animales::find()->select('animales.codigo_animal,protectoras.nombre_protectora,nombre_mascota,tipo_de_animal,edad,raza,cartillas.chip,cartillas.castrado,cartillas.incidentes,adopcion')
                                       ->innerjoin('protectoras','protectoras.codigo = animales.codigo')
                                       ->where('apto_para_adoptar = 1 and formulario.codigo_animal is null') 
                                       ->innerjoin('cartillas','cartillas.codigo_animal = animales.codigo_animal')
                                       ->leftJoin("formulario", $on="formulario.codigo_animal=animales.codigo_animal"),
                                       
                                       
            'pagination' => false,
            ]);
        return $this->render('todoslosanimales',[
         "dataProvider"=>$dataProvider , 
        
        ]); 
    } 
       ////////////////////////////////////////////////////////////////////////// parte de codigo PDF /////////////////////////////////////////////////////////////////////////
// 
//    public function actionReport($id){ 
//        
//        $model= Formulario::findModel($id);
//        
//        $protectora=\yii::$app->db->createCommand('SELECT concat(nombre_protectora,"<br>", telefono,"<br>", direccion) FROM protectoras where codigo = ' . $model->codigo)->queryScalar();
//        $html = $this->renderPartial('_reportView',['model'=>$model,'protectora' => $protectora]);
//        
//        $pdf = yii::$app->pdf;
//        $pdf->content=$html;
//        return $pdf->render();
        
        
            
//            $content = $this->renderPartial('_ReportView');
//            $pdf = yii::$app->pdf;
//            
//            $mpdf = $pdf->api;
//            
//            $mpdf->Bookmark('inicio');
//            $mpdf-> writeHTML('<div>hola mundo</div>');
//            $mpdf->Output();
//            return $mpdf->render();
//    }
//    
       ////////////////////////////////////////////////////////////////////////// DESPLEGABLE  ANIMALES DEVUELTOS  /////////////////////////////////////////////////////////////////////////
       public function actionOpcion() {

        $dataProvider = new ActiveDataProvider([
            'query' => Animales::find()->select('animales.codigo_animal,protectoras.nombre_protectora,nombre_mascota,tipo_de_animal,edad,raza,cartillas.chip,adopcion')
                    ->innerjoin('protectoras', 'protectoras.codigo = animales.codigo')
                    ->where('adopcion = 1 and formulario.codigo_animal is null')
                    ->innerjoin('cartillas', 'cartillas.codigo_animal = animales.codigo_animal')
                    ->leftJoin("formulario", $on="formulario.codigo_animal=animales.codigo_animal"),
                    
            
            'pagination' => false,
        ]);
        return $this->render('animalesDevueltos', [
                    "dataProvider" => $dataProvider,
        ]);
    }
 

    public function actionOpcion1() {

        $dataProvider = new ActiveDataProvider([
            'query' => Animales::find()->select('animales.codigo_animal,protectoras.nombre_protectora,nombre_mascota,tipo_de_animal,edad,raza,cartillas.chip,adopcion')
                    ->innerjoin('protectoras', 'protectoras.codigo = animales.codigo')
                    ->where('tipo_de_animal = "perro" and formulario.codigo_animal is null')
                    ->andwhere('adopcion =1')
                    ->innerjoin('cartillas', 'cartillas.codigo_animal = animales.codigo_animal')
                    ->leftJoin("formulario", $on="formulario.codigo_animal=animales.codigo_animal"),
            
            'pagination' => false,
        ]);
        return $this->render('animalesDevueltos', [
                    "dataProvider" => $dataProvider,
        ]);
    }
    
       public function actionOpcion2() {

        $dataProvider = new ActiveDataProvider([
            'query' => Animales::find()->select('animales.codigo_animal,protectoras.nombre_protectora,nombre_mascota,tipo_de_animal,edad,raza,cartillas.chip,adopcion')
                    ->innerjoin('protectoras', 'protectoras.codigo = animales.codigo')
                    ->where('tipo_de_animal = "gato" and formulario.codigo_animal is null')
                ->andwhere('adopcion =1')
                    ->innerjoin('cartillas', 'cartillas.codigo_animal = animales.codigo_animal')
                    ->leftJoin("formulario", $on="formulario.codigo_animal=animales.codigo_animal"),
            
            'pagination' => false,
        ]);
        return $this->render('animalesDevueltos', [
                    "dataProvider" => $dataProvider,
        ]);
    }
//    
//       public function actionOpcion3() {
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => Animales::find()->select('protectoras.nombre_protectora,nombre_mascota,tipo_de_animal,edad,raza,cartillas.chip')
//                    ->innerjoin('protectoras', 'protectoras.codigo = animales.codigo')
//                    ->where('apto_para_adoptar = 1')
//                    ->innerjoin('cartillas', 'cartillas.codigo_animal = animales.codigo_animal'),
//            'pagination' => false,
//        ]);
//        return $this->render('animalesDevueltos', [
//                    "dataProvider" => $dataProvider,
//        ]);
//    }
//       public function actionOpcion4() {
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => Animales::find()->select('protectoras.nombre_protectora,nombre_mascota,tipo_de_animal,edad,raza,cartillas.chip')
//                    ->innerjoin('protectoras', 'protectoras.codigo = animales.codigo')
//                     ->where('raza = "boxer"')
//                    ->andwhere('tipo_de_animal = "perro"')
//                    ->innerjoin('cartillas', 'cartillas.codigo_animal = animales.codigo_animal'),
//            'pagination' => false,
//        ]);
//        return $this->render('animalesDevueltos', [
//                    "dataProvider" => $dataProvider,
//        ]);
//    }
//    
//      public function actionOpcion5() {
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => Animales::find()->select('protectoras.nombre_protectora,nombre_mascota,tipo_de_animal,edad,raza,cartillas.chip')
//                    ->innerjoin('protectoras', 'protectoras.codigo = animales.codigo')
//                    ->where('adopcion = 1')
//                    ->innerjoin('cartillas', 'cartillas.codigo_animal = animales.codigo_animal'),
//            'pagination' => false,
//        ]);
//        return $this->render('animalesDevueltos', [
//                    "dataProvider" => $dataProvider,
//        ]);
//    }
//    public function acationFormulario($mensaje = null){
//        
//        
//        
//        return $this->render("formulario",["mensaje" => $mensaje]);
//    }
//    
//    public function actionRequest(){
//        
//        $mensaje = null;
//        
//        if(isset($_REQUEST["nombre"])){
//            
//            $mensaje = "bien, has enviado tu nombre correctamente: " . $_REQUEST["nombre"];
//        }
//        $this->redirect(["site/formulario","mensaje" => $mensaje]);
//    }
//    
//    
//    
    
    
    
    
    
    public function actionPruebamodal(){
        
           $dataProvider = new ActiveDataProvider([
            'query' => Animales::find()->select('edad,raza,cartillas.chip,cartillas.incidentes')
                    ->innerjoin('protectoras', 'protectoras.codigo = animales.codigo')
                   
                    ->innerjoin('cartillas', 'cartillas.codigo_animal = animales.codigo_animal'),
            'pagination' => false,
        ]);
        return $this->render('animalesDevueltos', [
                    "dataProvider" => $dataProvider,
        ]);  
    }
    
    

    
       /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /*   public function actionListaanimales(){
        
        $dataProvider = new SqlDataProvider([
           'sql'=>"SELECT * FROM animales where codigo = '1'", 
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['codigo_animal','codigo','adopcion','tipo_de_animal','apto_para_adoptar','edad','raza'],
        ]);
    }
    */
    
    
 /*   public function actionPrueba(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => animales::find()-> where("codigo = 2"),
        ]);
         return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['codigo_animal','codigo','adopcion','tipo_de_animal','apto_para_adoptar','edad','raza'],
        ]); 
    } */
    
//    public function actionListaanimales2(){
//        
//        $dataProvider = new SqlDataProvider([
//           'sql'=>"SELECT codigo_animal,codigo,adopcion,tipo_de_animal,apto_para_adoptar,edad,raza FROM animales where codigo = '2'", 
//           
//        ]);
//        
//        return $this->render("resultado",[
//           "resultados"=>$dataProvider,
//           "campos"=>['codigo_animal','codigo','adopcion','tipo_de_animal','apto_para_adoptar','edad','raza'],
//        ]);
//    }
//    public function actionListaanimales3(){
//        
//        $dataProvider = new SqlDataProvider([
//           'sql'=>"SELECT * FROM animales where codigo = '3'", 
//        ]);
//        
//        return $this->render("resultado",[
//           "resultados"=>$dataProvider,
//           "campos"=>['codigo_animal','codigo','adopcion','tipo_de_animal','apto_para_adoptar','edad','raza'],
//        ]);
//    }
//    public function actionListaanimales4(){
//        
//        $dataProvider = new SqlDataProvider([
//           'sql'=>"SELECT * FROM animales where codigo = '4'", 
//        ]);
//        
//        return $this->render("resultado",[
//           "resultados"=>$dataProvider,
//           "campos"=>['codigo_animal','codigo','adopcion','tipo_de_animal','apto_para_adoptar','edad','raza'],
//        ]);
//    }
//    public function actionListaanimales5(){
//        
//        $dataProvider = new SqlDataProvider([
//           'sql'=>"SELECT * FROM animales where codigo = '5'", 
//        ]);
//        
//        return $this->render("resultado",[
//           "resultados"=>$dataProvider,
//           "campos"=>['codigo_animal','codigo','adopcion','tipo_de_animal','apto_para_adoptar','edad','raza'],
//        ]);
//    }
//    public function actionListaanimales6(){
//        
//        $dataProvider = new SqlDataProvider([
//           'sql'=>"SELECT * FROM animales where codigo = '6'", 
//        ]);
//        
//        return $this->render("resultado",[
//           "resultados"=>$dataProvider,
//           "campos"=>['codigo_animal','codigo','adopcion','tipo_de_animal','apto_para_adoptar','edad','raza'],
//        ]);
//    }
    
//    public function actionRegistro()
//    {
//        $model = new usuarios();
//
//        if ($model->load(Yii::$app->request->post())) {
//            if ($model->validate()) {
//                // form inputs are valid, do something here
//               $model -> nombre = $_POST['Usuarios']['nombre'];
//               $model -> email = $_POST['Usuarios']['email'];
//               $model -> password = password_hash($_POST['Usuarios']['password'], PASSWORD_ARGON2I);
//               $model -> authKey = md5(random_bytes(5));
//               $model -> accessToken = password_hash(random_bytes(10), PASSWORD_DEFAULT); 
//               if($model->save()){
//                  return $this->redirect(['login']);
//               }
//            }
//        }
//
//        return $this->render('registro', [
//            'model' => $model,
//        ]);
//    }
    
    
    
}
